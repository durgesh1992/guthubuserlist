package com.example.githubuser.db


import androidx.room.TypeConverter
import com.example.githubuser.model.Item
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object ResponseTypeConverters {
    @TypeConverter
    @JvmStatic
    fun convertListToString(value: List<Item>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Item>>() {}.type
        return gson.toJson(value, type)
    }
    @TypeConverter
    @JvmStatic
    fun convertStringIntoListofItem(data: String): List<Item>? {
        val gson = Gson()
        return gson.fromJson<List<Item>>(data, object : TypeToken<List<Item>>() {

        }.type)
    }




}
