package com.example.githubuser.repos

import com.example.githubuser.model.GitHubUserResponse

import com.example.githubuser.api.APIInteface
import com.example.githubuser.api.Failure
import com.example.githubuser.api.Result
import com.example.githubuser.api.Success
import com.example.githubuser.model.GithubIssueCommentModel
import com.example.githubuser.model.GithubIssueListModel

import org.json.JSONObject
import javax.inject.Inject


class APIRepository @Inject constructor(val apiInteface: APIInteface)
{
        val SUCCESS: Int = 200;
        val FAILURE: Int = 400;
        val SERVER_ERROR: Int = 500;
    suspend fun callGithubAPI( map: Map<String, String>) : Result<GitHubUserResponse>
    {
        val request = apiInteface.getGitHubUserList("application/vnd.github.v3+json",map);
        val response = request.await();
        val githubResponse = response.body();
        if(response.code()==SUCCESS) {
            return Success(githubResponse!!);
        }
        else if(response.code()==FAILURE)
        {
            return Failure("API Failed");

        }
        else {
            val jObjError = JSONObject(response.errorBody()!!.string())
            if(jObjError.has("msg")) {
                return Failure(jObjError.getString("msg"));
            }
            return Failure("Server Error")
        }


    }

    suspend fun callGithubAPIIssueList() : Result<GithubIssueListModel>
    {
        val request = apiInteface.getIssueList()
        val response = request.await();
        val githubResponse = response.body();
        if(response.code()==SUCCESS) {
            return Success(githubResponse!!);
        }
        else if(response.code()==FAILURE)
        {
            return Failure("API Failed");

        }
        else {
            val jObjError = JSONObject(response.errorBody()!!.string())
            if(jObjError.has("msg")) {
                return Failure(jObjError.getString("msg"));
            }
            return Failure("Server Error")
        }


    }
    suspend fun callGithubAPIIssueCommentDetail(commentId:String) : Result<GithubIssueCommentModel>
    {
        val request = apiInteface.getCommentDetailOfIssue(commentId)
        val response = request.await();
        val githubResponse = response.body();
        if(response.code()==SUCCESS) {
            return Success(githubResponse!!);
        }
        else if(response.code()==FAILURE)
        {
            return Failure("API Failed");

        }
        else {
            val jObjError = JSONObject(response.errorBody()!!.string())
            if(jObjError.has("msg")) {
                return Failure(jObjError.getString("msg"));
            }
            return Failure("Server Error")
        }


    }


}