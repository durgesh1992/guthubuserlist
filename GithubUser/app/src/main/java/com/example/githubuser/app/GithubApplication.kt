package com.example.githubuser.app

import android.app.Activity
import android.app.Application
import com.example.githubuser.di.ActivityInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject



class GithubApplication :Application(),HasActivityInjector
{
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        ActivityInjector.init(this)




    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector;

    }


}