package com.example.githubuser.di


import com.example.githubuser.view.GithubCommentListActivity
import com.example.githubuser.view.GithubUserDetailActivity
import com.example.githubuser.view.GithubUserListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeGithubUserListActivity(): GithubUserListActivity

    @ContributesAndroidInjector
    abstract fun contributeGithubUserDetailActivity(): GithubUserDetailActivity
    @ContributesAndroidInjector
    abstract fun contributeGithubCommentListActivity(): GithubCommentListActivity

}