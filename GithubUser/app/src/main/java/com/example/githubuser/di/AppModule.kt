package com.example.githubuser.di

import android.app.Application
import android.content.Context
import androidx.annotation.NonNull
import androidx.room.Room
import com.example.githubuser.db.GitHubDb
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.example.githubuser.api.APIInteface
import com.example.githubuser.db.GithubUserDataDao
import com.example.githubuser.utils.ProgressDialogHandler
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {
    //val BASE_URL="https://api.github.com/"
    val BASE_URL = " https://api.github.com/repos/firebase/firebase-ios-sdk/"

    @Singleton
    @Provides
    fun provideContext(@NonNull application: Application): Context {
        return application
    }

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
//            .addNetworkInterceptor(StethoInterceptor())
            .callTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
        return client
    }

    @Singleton
    @Provides
    fun provideGithubService(okHttpClient: OkHttpClient): APIInteface {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(APIInteface::class.java)
    }

    @Singleton
    @Provides
    fun provideGithubDb(application: Application): GitHubDb {
        return Room.databaseBuilder(application, GitHubDb::class.java, "github.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideSelfDataDao(gitHubDb: GitHubDb): GithubUserDataDao {
        return gitHubDb.githubUserDataDao
    }

    @Singleton
    @Provides
    fun providesProgressLoader(): ProgressDialogHandler {
        return ProgressDialogHandler();
    }


}