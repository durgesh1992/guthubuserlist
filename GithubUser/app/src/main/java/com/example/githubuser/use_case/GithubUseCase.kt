package com.example.githubuser.use_case

import com.example.githubuser.api.Result
import com.example.githubuser.model.GitHubUserResponse
import com.example.githubuser.model.GithubIssueCommentModel
import com.example.githubuser.model.GithubIssueListModel

import com.example.githubuser.repos.APIRepository
import javax.inject.Inject

class GithubUseCase @Inject constructor(val apiRepository: APIRepository) {
    suspend fun getGithubUserList(
        map: Map<String, String>
    ): Result<GitHubUserResponse> {
        return apiRepository.callGithubAPI(map)
    }

    suspend fun getGithubUserIssueList(

    ): Result<GithubIssueListModel> {
        return apiRepository.callGithubAPIIssueList()
    }

    suspend fun getGithubUserIssueCommentDetail(
        commentId: String
    ): Result<GithubIssueCommentModel> {
        return apiRepository.callGithubAPIIssueCommentDetail(commentId)
    }

}