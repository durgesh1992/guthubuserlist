package com.example.githubuser.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo



/**
 * Class for check internet connection
 */
object InternetConnectionHelper {
    fun isConnected(context: Context): Boolean {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}
