
package com.example.githubuser.di
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.githubuser.model.GithubIssueListViewModel
import com.example.githubuser.model.GithubUserListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(GithubUserListViewModel::class)
    abstract fun bindGithubUserListViewModel(githubUserListViewModel: GithubUserListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(myViewModelFactory: MyViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(GithubIssueListViewModel::class)
    abstract fun bindGithubIssueListViewModel(githubIssueListViewModel: GithubIssueListViewModel): ViewModel

}
